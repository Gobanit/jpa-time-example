package com.randakm.sandbox.jpatimeexample.controller;

import com.randakm.sandbox.jpatimeexample.data.AbstractEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;

@RestController
@RequestMapping("main")
public class MainController {
    @Autowired
    private EntityManager em;

    @Transactional
    @GetMapping("create")
    public AbstractEntity createNew() {
        System.out.println("create");
        System.out.println("ZoneId=" + ZoneId.systemDefault());
        var e = new AbstractEntity();
        em.persist(e);
        return e;
    }

    @Transactional
    @GetMapping("before")
    public List<AbstractEntity> listAllBefore() {
        var query = em.createQuery("SELECT e FROM AbstractEntity e WHERE e.createdAt < :threshold", AbstractEntity.class);
        var now = Instant.now();
        System.out.println("Entities before=" + now);
        query.setParameter("threshold", now);
        return query.getResultList();
    }

    @Transactional
    @GetMapping("after")
    public List<AbstractEntity> listAllAfter() {
        var query = em.createQuery("SELECT e FROM AbstractEntity e WHERE e.createdAt > :threshold", AbstractEntity.class);
        var now = Instant.now();
        System.out.println("Entities after=" + now);
        query.setParameter("threshold", now);
        return query.getResultList();
    }

    @GetMapping("zone")
    public ZoneId currentZone() {
        return ZoneId.systemDefault();
    }
}
