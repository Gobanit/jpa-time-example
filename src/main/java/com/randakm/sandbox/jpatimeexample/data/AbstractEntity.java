package com.randakm.sandbox.jpatimeexample.data;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
public class AbstractEntity {
    @Id
    @GeneratedValue
    private Long id;
    //    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private Instant createdAt;
    //    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime createdAtZoned;
    private LocalDateTime createdAtLocal;
    private Instant modifiedAt;

    @PrePersist
    protected void prePersist() {
        createdAt = Instant.now();
        modifiedAt = createdAt;
        createdAtZoned = createdAt.atZone(ZoneId.systemDefault());
        createdAtLocal = createdAtZoned.toLocalDateTime();
        System.out.println(String.format("Created: instant=%s, zoned=%s, local=%s", createdAt, createdAtZoned, createdAtLocal));
    }

    @PreUpdate
    protected void preUpdate() {
        modifiedAt = Instant.now();
    }
}
